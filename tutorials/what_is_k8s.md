# Kubernetes....what the hell is it and why should I care?

## System overview
Kubernetes or (k8s) for short is a software platform that abstracts the creation/deployment process of your containerized applications. 
K8s is a stateful platform. It keeps track of its current state (what containers are running, how many copies, port mappings etc..), you then tell the k8s api what you _want_ the cluster to look like, and the api server goes out and pushes the cluster to that state.

### Components
There are the main components to k8s
- The master (Consists of the API server, Controller manager, Scheduler and Etcd)
- The worker nodes (Consists of the kubelet, docker, )